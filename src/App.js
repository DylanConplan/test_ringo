import React, { Component } from 'react'; 
import './App.css';
import firebase from "firebase";
import Home from "./Home";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" 
integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" 
crossorigin="anonymous"></link>

firebase.initializeApp({
    apiKey: "AIzaSyB1mNA5AoAOYjj9RthLiaKMr9QX_yBVYR4",
    authDomain: "prueba-554e4.firebaseapp.com"
});

class App extends Component {
    state = {isSignedIn: false}
    uiConfig = {
        signInFlow: "popup",
        signInOptions: [
            firebase.auth.GoogleAuthProvider.PROVIDER_ID,
            firebase.auth.EmailAuthProvider.PROVIDER_ID
        ],
        callbacks: {
            signInSuccess: () => false
        }
    }
    componentDidMount(){

        firebase.auth().onAuthStateChanged(user =>{
            this.setState({isSignedIn:!!user});
            console.log(user);
        });
       
    }
    
  render() {
    return (
      <div className="App">
       {this.state.isSignedIn ? (
       <Home/>) : (<StyledFirebaseAuth
        uiConfig={this.uiConfig}
        firebaseAuth={firebase.auth()}
        />
        )}
      </div>
    );
  }
}

export default App;
